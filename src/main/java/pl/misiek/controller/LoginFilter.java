package pl.misiek.controller;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.Map;

@WebFilter(urlPatterns = {"/filter"})
public class LoginFilter implements Filter {

    public static final String USER = "user";
    private static final String UNKNOWN_USER_NAME = "";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        Map<String, String[]> parameterMap = servletRequest.getParameterMap();
        if (parameterMap.containsKey(USER)) {
            servletRequest.setAttribute(USER, parameterMap.get(USER) [0]);
        } else {
            servletRequest.setAttribute(USER, UNKNOWN_USER_NAME);
        }

        filterChain.doFilter(servletRequest, servletResponse);

    }
}
