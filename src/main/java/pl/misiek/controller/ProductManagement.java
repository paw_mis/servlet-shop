package pl.misiek.controller;


import pl.misiek.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(urlPatterns = {"/productManagement"})
public class ProductManagement extends HttpServlet {


    private static final String NAME = "name";
    private static final String ILOSC = "ilosc";
    private static final String RODZAJ = "rodzaj";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        writer.println("Test");

    }




    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Product product = new Product();
        product.setName(request.getParameter(NAME));
        product.setIlosc(Integer.parseInt(request.getParameter(ILOSC)));
        product.setRodzaj(request.getParameter(RODZAJ));
        PrintWriter writer = response.getWriter();
        writer.println(product.getName());

    }
}
